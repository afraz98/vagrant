class_name Rifleman extends Enemy

@onready var rifleman_sprite = get_node("RiflemanSprite") as Sprite2D
@onready var rifle = get_node("Rifle") as Rifle

@onready var rifleman_crouching_hitbox = get_node("RiflemanCrouchingHitbox") as CollisionShape2D
@onready var rifleman_standing_hitbox = get_node("RiflemanStandingHitbox") as CollisionShape2D

var player_dead: bool = false

func aim():
	_state_machine.transition_to_state("aim")

func _on_player_death():
	player_dead = true
	
func _ready():
	walking_direction = -1.0
	_state_machine = get_node("RiflemanStateMachine") as RiflemanStateMachine
	_state_machine.set_parent(self)
	_state_machine.enemy_shot.connect(shoot)
	_state_machine.enemy_reloaded.connect(reload)
	pass

func out_of_ammo() -> bool:
	return rifle.is_empty()

func get_player_proximity():
	return self.position.x - player_position

func shoot():
	return rifle.shoot(-rifleman_sprite.scale.x)

func reload():
	rifle.reload()

func _physics_process(delta):
	# Update player proximity
	_state_machine.update_player_proximity(get_player_proximity())
	
	# Process current state
	_state_machine.update()
	_state_machine.physics_update(delta)

	velocity.x = _state_machine.get_velocity().x
	velocity.y = _state_machine.get_velocity().y
	
	if is_on_wall():
		velocity.x = -velocity.x
	
	# Flip sprite if moving left		
	if velocity.x != 0:
		rifleman_sprite.flip_h = velocity.x > 0

	# Clamp to the maximum horizontal movement speed.
	velocity.x = clamp(velocity.x, -WALK_MAX_SPEED, WALK_MAX_SPEED)

	# Move based on the velocity and snap to the ground.
	move_and_slide()
		
	rifleman_sprite.play_animation(_state_machine.get_animation_name())
	pass
