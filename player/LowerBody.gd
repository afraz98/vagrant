class_name LowerBody extends Sprite2D

@onready var animation_player = get_node("AnimationPlayer") as AnimationPlayer

func play_animation(animation: String) -> void:
	animation_player.play(animation)
