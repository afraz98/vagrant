class_name StationaryEnemy extends Enemy
@onready var stationary_enemy_sprite: Sprite2D = get_node("StationaryEnemySprite") as Sprite2D

func _enemy_died_nearby(enemy: Enemy):
	if !self.is_dead():
		if abs(enemy.position.x - self.position.x) < 100:
			_state_machine.transition_to_state("fear")
	pass

func _ready():
	_state_machine = get_node("StationaryEnemyStateMachine") as StationaryEnemyStateMachine
	walking_direction = -1.0
	pass

func _on_player_death():
	pass

func _physics_process(delta):
	_state_machine.update()
	_state_machine.physics_update(delta)

	velocity.x = _state_machine.get_velocity().x
	velocity.y = _state_machine.get_velocity().y

	if is_on_wall():
		velocity.x = -velocity.x

	velocity.x = velocity.x * stationary_enemy_sprite.scale.x

	# Flip sprite if moving left
	if velocity.x != 0:
		stationary_enemy_sprite.flip_h = velocity.x > 0

	# Clamp to the maximum horizontal movement speed.
	velocity.x = clamp(velocity.x, -WALK_MAX_SPEED, WALK_MAX_SPEED)
	
	# Move based on the velocity and snap to the ground.
	move_and_slide()

	stationary_enemy_sprite.play_animation(_state_machine.get_animation_name())
	pass
