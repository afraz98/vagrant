class_name RiflemanSprite extends Sprite2D

var animation_timeouts = {
	'aim': 0.1,
	'alert': 0.1,
	'idle': 0.1,
	'reload': 2.5,
	'stand': 1.2,
	'death': 1.0,
	'shoot': 1.0,
}

@onready var animation_player = get_node("AnimationPlayer") as AnimationPlayer
@onready var timer = get_node("Timer") as Timer

func play_animation(animation: String) -> void:
	if timer.is_stopped() and animation != animation_player.current_animation:
		timer.wait_time = animation_timeouts[animation]
		timer.start()
		animation_player.play(animation)

