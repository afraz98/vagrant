class_name PlayerStateReloadingWalk extends PlayerState

signal player_reloaded()

func _ready():
	_id = "reload_idle"
	_upper_animation = "reload"
	_lower_animation = "walk"

func reset():
	pass

func update():
	player_reloaded.emit()
	transition_to_state("walk")
	pass
