class_name PlayerStateAttacking extends PlayerState

signal player_shot()

func _ready():
	_id = "attacking"
	_animation = "shoot"
	pass
