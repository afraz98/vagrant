class_name StationaryEnemyStateMachine extends StateMachine

@onready var stationary_enemy_state_idle = get_node("StationaryEnemyStateIdle") as StationaryEnemyStateIdle
@onready var stationary_enemy_state_fear = get_node("StationaryEnemyStateFear") as StationaryEnemyStateFear
@onready var stationary_enemy_state_flee = get_node("StationaryEnemyStateFlee") as StationaryEnemyStateFlee
@onready var stationary_enemy_death_state = get_node("StationaryEnemyDeathState") as StationaryEnemyDeathState

@onready var state_timer = get_node("StateTimer") as Timer

func _ready():
	_state = stationary_enemy_state_idle
	_state.transition_state.connect(_on_transition_state)
	pass

func _on_transition_state(state_id: String):
	# Disconnect current transition signal
	_state.transition_state.disconnect(_on_transition_state)

	if state_id == "idle":
		_state = stationary_enemy_state_idle
	elif state_id == "fear":
		_state = stationary_enemy_state_fear
	elif state_id == "flee":
		_state = stationary_enemy_state_flee
	elif state_id == "death":
		_state = stationary_enemy_death_state
	
	# Connect new transition signal
	_state.transition_state.connect(_on_transition_state)
	
	# Start state timer
	start_state_timer(_state.get_wait_time())
	
	# Call state's reset function before processing begins
	_state.reset()	
	pass
	
func start_state_timer(wait_time):
	if wait_time > 0:
		state_timer.wait_time = wait_time
		state_timer.timeout.connect(_state._on_timer_timeout)
		state_timer.start()
	pass

func update_player_position(player_position):
	_state.update_player_position(player_position)

func get_velocity():
	return _state.velocity
