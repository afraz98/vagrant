class_name DroneStateMachine extends StateMachine

@onready var state_idle = get_node("DroneStateIdle") as DroneStateIdle
@onready var state_dying = get_node("DroneStateDying") as DroneStateDying

func _ready():
	_state = state_idle
	pass

func _process(delta):
	pass

func get_velocity():
	return _state.velocity

func _on_transition_state(state_id: String):
	if state_id == "idle":
		_state = state_idle
	elif state_id == "death":
		_state = state_dying
	
	_state.reset()
	pass
