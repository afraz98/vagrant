class_name DoorStateMachine extends StateMachine

@onready var door_state_closed = get_node("DoorStateClosed") as DoorStateClosed
@onready var door_state_opening = get_node("DoorStateOpening") as DoorStateOpening
@onready var door_state_open = get_node("DoorStateOpen") as DoorStateOpen

func _ready():
	_state = door_state_closed
	_state.transition_state.connect(_on_transition_state)
	pass

func _on_transition_state(state_id: String):
	# Disconnect current transition signal
	_state.transition_state.disconnect(_on_transition_state)

	if state_id == "opening":
		_state = door_state_opening
	elif state_id == "closed":
		_state = door_state_closed
	elif state_id == "open":
		_state = door_state_open

	_state.transition_state.connect(_on_transition_state)
	_state.reset()

func update_player_proximity(player_proximity: float) -> void:
	_state.update_player_proximity(player_proximity)

func update_player_position(player_position) -> void:
	_state.update_player_position(player_position)
