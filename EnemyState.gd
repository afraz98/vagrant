class_name EnemyState extends State

@onready var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")

var _upper_animation: String = ""
var _lower_animation: String = ""
var velocity: Vector2 = Vector2.ZERO
var is_on_floor: bool = false
var player_proximity: float

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func update_player_proximity(player_proximity):
	self.player_proximity = player_proximity
	pass

func get_upper_animation_name():
	return _upper_animation

func get_lower_animation_name():
	return _lower_animation

func set_is_on_floor(on_floor: bool) -> void:
	is_on_floor = on_floor

func physics_update(delta):
	# Vertical movement code. Apply gravity.
	velocity.y += gravity * delta

