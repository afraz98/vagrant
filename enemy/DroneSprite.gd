class_name DroneSprite extends Sprite2D

@onready var animation_player = get_node("AnimationPlayer") as AnimationPlayer
@onready var timer = get_node("Timer") as Timer

var animation_timeouts = {
	'attack': 0.4,
	'idle': 0.4,
	'death': 0.45,
	'walk': 0.4,
}

func play_animation(animation: String) -> void:
	if timer.is_stopped() and animation != animation_player.current_animation:
		timer.wait_time = animation_timeouts[animation]
		timer.start()
		animation_player.play(animation)

