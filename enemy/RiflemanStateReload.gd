class_name RiflemanStateReload extends EnemyState

signal enemy_reloaded()

func _ready():
	_id = "reload"
	_animation = "reload"
	
func update():
	enemy_reloaded.emit()
	transition_to_state("alert")
	pass
