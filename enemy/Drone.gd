class_name Drone extends Enemy

@onready var drone_sprite = get_node("DroneSprite") as DroneSprite

func _ready():
	_state_machine = get_node("DroneStateMachine") as DroneStateMachine
	health = 10
	pass
	
func _on_hit():
	if health <= 0:
		_state_machine.transition_to_state("death")
	else:
		health = health - 1
	pass
	
func _physics_process(delta):
	_state_machine.update()
	_state_machine.physics_update(delta)
	
	velocity.x = _state_machine.get_velocity().x
	velocity.y = _state_machine.get_velocity().y
	
	move_and_slide()
	drone_sprite.play_animation(_state_machine.get_animation_name())
	pass
	
