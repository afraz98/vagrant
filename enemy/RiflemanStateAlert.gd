class_name RiflemanStateAlert extends EnemyState

func _ready():
	_id = "alert"
	_animation = "alert"
	pass

func out_of_ammo() -> bool:
	return _parent.out_of_ammo()

func update():
	if out_of_ammo():
		transition_to_state("reload")
	
	if player_proximity > 200:
		transition_to_state("idle")

	transition_to_state("aim")
	pass
	
func physics_update(delta):
	pass
