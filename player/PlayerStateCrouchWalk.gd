class_name PlayerStateCrouchWalk extends PlayerState

const CROUCH_FORCE = 300
const CROUCH_MAX_SPEED = 100

func _ready():
	_id = "crouch_walk"
	_animation = "crouch_walk"
	full_body_animation = true
	velocity = Vector2.ZERO
	pass
	
func reset():
	velocity = Vector2.ZERO

func physics_update(delta):
	velocity.y += gravity * delta 
	pass

func update():	
	if Input.is_action_just_released("crouch"):
		if Input.is_action_pressed("move_right") or Input.is_action_just_pressed("move_left"):
			transition_to_state("walk")
		else:
			transition_to_state("idle")

	if Input.is_action_just_released("move_right") or Input.is_action_just_released("move_left"):
		transition_to_state("crouch_idle")

	if Input.is_action_pressed("shoot"):
		transition_to_state("shooting_crouched")
	
	velocity.x = CROUCH_MAX_SPEED * Input.get_axis(&"move_left", &"move_right")
	pass

