class_name UserInterface extends Control

@onready var ammo_label = get_node("UserInterfacePanel/AmmoLabel")

func _ready():
	pass

func set_ammo_count(current_ammo_count):
	ammo_label.set_text("%s" % [current_ammo_count])
