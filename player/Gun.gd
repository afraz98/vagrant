class_name Gun extends Marker2D

const bullet = preload("res://environment/Bullet.tscn")
const BULLET_VELOCITY = 500.0

@onready var reload_timer = get_node("ReloadTimer") as Timer
@onready var cooldown_timer = get_node("CooldownTimer") as Timer

# Maximum ammo capacity for the pistol
const PISTOL_MAGAZINE_CAPACITY = INF
var ammo := PISTOL_MAGAZINE_CAPACITY

func _ready() -> void:
	ammo = PISTOL_MAGAZINE_CAPACITY
	pass

func reload() -> bool:
	if not reload_timer.is_stopped():
		# In the middle of a reload already -- cannot reload.
		return false
	reload_timer.start()
	ammo = PISTOL_MAGAZINE_CAPACITY
	return true

func shoot(direction: float, is_aiming_up: bool) -> bool:
	if ammo == 0:
		# TODO: Probably best to play some form of sound effect
		# indicating the player is out of ammo.
		return false
	
	if not cooldown_timer.is_stopped():
		# Already shooting -- wait for cooldown to elapse.
		return false
		
	if not reload_timer.is_stopped():
		# In the middle of reloading -- should not be able to shoot.
		return false
	
	var projectile := bullet.instantiate() as Bullet
		
	projectile.global_position = global_position
	projectile.initial_position = global_position.x
	
	if is_aiming_up:
		projectile.linear_velocity = Vector2(0.0 , -BULLET_VELOCITY)
		projectile.rotation_degrees = -90
	else:
		projectile.linear_velocity = Vector2(direction * BULLET_VELOCITY, 0.0)
		projectile.scale.x = direction
	projectile.set_as_top_level(true)
	add_child(projectile)
	
	# Decrease current ammo
	ammo -= 1
	
	cooldown_timer.start()
	return true
