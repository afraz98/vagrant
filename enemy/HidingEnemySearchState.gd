class_name HidingEnemySearchState extends EnemyState

func _ready():
	_id = "search"
	_animation = "search"
	pass

func update():
	if abs(self.player_proximity) <= 50:
		transition_to_state("fear")
	pass
	


