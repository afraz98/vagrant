class_name Grenade extends RigidBody2D

const explosion = preload("res://environment/Explosion.tscn")
@onready var fuse = get_node("Fuse") as Timer
@onready var animation_player = get_node("AnimationPlayer") as AnimationPlayer

signal spawn_explosion(position: Vector2)

func _ready():
	fuse.timeout.connect(_on_timer_timeout)
	fuse.start()
	
func _on_timer_timeout():
	destroy()

func play_animation(animation) -> void:
	$AnimationPlayer.play(animation)

func explode() -> void:
	spawn_explosion.emit(global_position)
	queue_free()
	pass

func destroy() -> void:
	play_animation("destroy")

