class_name RiflemanStateIdle extends EnemyState

func _ready():
	_id = "idle"
	_animation = "idle"
	velocity = Vector2.ZERO
	player_proximity = INF
	pass

func reset():
	velocity = Vector2.ZERO
	player_proximity = INF

func update():
	if player_proximity <= 200:
		transition_to_state("alert")
	pass
