class_name RiflemanStateAim extends EnemyState

func _ready():
	_id = "aim"
	_animation = "aim"
	cycles = 0
	pass
	
func reset():
	cycles = 0

func shoot():
	return _parent.shoot()


func update():	
	cycles += 1
	if cycles >= 60:
		if(shoot()):
			transition_to_state("shoot")
	pass

