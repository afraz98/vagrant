class_name HidingEnemyStateMachine extends StateMachine

@onready var hiding_enemy_state_idle = get_node("HidingEnemyIdleState") as HidingEnemyIdleState
@onready var hiding_enemy_state_fear = get_node("HidingEnemyFearState") as HidingEnemyFearState
@onready var hiding_enemy_state_search  = get_node("HidingEnemySearchState") as HidingEnemySearchState
@onready var hiding_enemy_state_flee = get_node("HidingEnemyFleeState") as HidingEnemyFleeState
@onready var hiding_enemy_death_state = get_node("HidingEnemyDeathState") as HidingEnemyDeathState

@onready var state_timer = get_node("StateTimer") as Timer

func _ready():
	_state = hiding_enemy_state_search
	_state.transition_state.connect(_on_transition_state)
	pass

func update_player_proximity(player_proximity: float) -> void:
	_state.update_player_proximity(player_proximity)

func start_state_timer(wait_time):
	if wait_time > 0:
		state_timer.wait_time = wait_time
		state_timer.timeout.connect(_state._on_timer_timeout)
		state_timer.start()
	pass

func _on_transition_state(state_id: String):
	# Disconnect current transition signal
	_state.transition_state.disconnect(_on_transition_state)

	if state_id == "idle":
		_state = hiding_enemy_state_idle
	elif state_id == "fear":
		_state = hiding_enemy_state_fear
	elif state_id == "search":
		_state = hiding_enemy_state_search
	elif state_id == "flee":
		_state = hiding_enemy_state_flee
	elif state_id == "death":
		_state = hiding_enemy_death_state
	
	# Connect new transition signal
	_state.transition_state.connect(_on_transition_state)
	
	# Start state timer
	start_state_timer(_state.get_wait_time())
	
	# Call state's reset function before processing begins
	_state.reset()	
	pass

func get_velocity():
	return _state.velocity
