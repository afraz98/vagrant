class_name PlayerStateCrouched extends PlayerState

func _ready():
	_id = "crouch_idle"
	_animation = "crouch_idle"
	pass

func reset():
	velocity = Vector2.ZERO
	full_body_animation = true
	pass

func update():
	if Input.is_action_just_released("crouch"):
		transition_to_state("idle")

	if Input.is_action_pressed("jump"):
		transition_to_state("jump")

	if Input.is_action_pressed("move_right") or Input.is_action_pressed("move_left"):
		transition_to_state("crouch_walk")

	if Input.is_action_pressed("shoot"):
		transition_to_state("shooting_crouched")
	pass
	
func physics_update(delta):
	# Vertical movement code. Apply gravity.
	velocity.y += gravity * delta
