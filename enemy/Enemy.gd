class_name Enemy extends CharacterBody2D

@onready var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")

var _state_machine: StateMachine

signal enemy_killed(enemy)
var player_position: int = 0
var health: int = 0

const WALK_FORCE = 600			# Acceleration for walking
const WALK_MAX_SPEED = 150 		# Maximum x velocity while walking
const CROUCH_FORCE = 300 		# Crouching acceleration
const CROUCH_MAX_SPEED = 100 	# Maximum x velocity while crouched
const STOP_FORCE = 1300 		# Stopping [de]acceleration
const JUMP_SPEED = 500  		# Maximum y velocity while jumping

var walking_direction = 0
var initial_position_x = 0
	
func _on_hit() -> void:
	_state_machine.transition_to_state("death")

func is_dead():
	return _state_machine.get_current_state() == "death"
	
func _enemy_died_nearby(enemy: Enemy):
	pass
	
func broadcast_death():
	enemy_killed.emit(self)

func update_player_position(pos_x):
	player_position = pos_x
		
func _physics_process(delta):
	if is_on_wall():
		velocity.x = -velocity.x
	
	# Flip sprite if moving left
	if velocity.x != 0:
		$EnemySprite.flip_h = velocity.x > 0

	# Clamp to the maximum horizontal movement speed.
	velocity.x = clamp(velocity.x, -WALK_MAX_SPEED, WALK_MAX_SPEED)

	# Vertical movement code. Apply gravity.
	velocity.y += gravity * delta

	# Move based on the velocity and snap to the ground.
	move_and_slide()
	pass
