class_name PlayerStateWalk extends PlayerState

const WALK_FORCE = 600
const WALK_MAX_SPEED = 200

func _ready():
	_id = "walk"
	_animation = "walk"
	_upper_animation = "walk"
	_lower_animation = "walk"
	velocity = Vector2.ZERO
	pass

func reset():
	pass

func physics_update(delta):
	velocity.y += gravity * delta

func update():	
	if Input.is_action_pressed("crouch"):
		transition_to_state("crouch_walk")

	if Input.is_action_pressed("jump"):
		transition_to_state("jump")

	if Input.is_action_pressed("reload"):
		transition_to_state("reload_walk")

	if Input.is_action_pressed("shoot"):
		transition_to_state("shooting_walk")
	
	if Input.is_action_just_released("move_right") or Input.is_action_just_released("move_left"):
		transition_to_state("idle")	
	
	velocity.x = WALK_MAX_SPEED * Input.get_axis(&"move_left", &"move_right")
	pass

