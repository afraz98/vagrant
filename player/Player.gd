class_name Player extends CharacterBody2D

@onready var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")

signal player_moved(x: int)
signal gun_shot()
signal player_reloaded()
signal player_dead()

@onready var upper_body 			= get_node("UpperBody") as UpperBody
@onready var lower_body 			= get_node("LowerBody") as LowerBody
@onready var gun 					= get_node("Gun") as Gun
@onready var grenade_thrower		= get_node("GrenadeThrower") as GrenadeThrower
@onready var full_body 				= get_node("FullBody") as FullBody
@onready var player_state_machine 	= get_node("PlayerStateMachine") as PlayerStateMachine
@onready var player_hitbox 			= get_node("PlayerHitbox") as CollisionShape2D
@onready var player_crouch_hitbox 	= get_node("PlayerCrouchHitbox") as CollisionShape2D

func _ready() -> void:
	player_state_machine.player_shot.connect(_on_player_shot)
	player_state_machine.player_reloaded.connect(_on_player_reloaded)
	player_state_machine.player_dead.connect(_on_player_dead)
	player_state_machine.player_throw.connect(_on_player_throw)
	pass

func switch_full_body(toggle: bool) -> void:
	if toggle:
		upper_body.set_visible(false)
		lower_body.set_visible(false)
		full_body.set_visible(true)	
	else:
		upper_body.set_visible(true)
		lower_body.set_visible(true)
		full_body.set_visible(false)

func update_hitbox(toggle: bool) -> void:
	if toggle:
		player_hitbox.disabled = true
		player_crouch_hitbox.disabled = false
	else:
		player_hitbox.disabled = false
		player_crouch_hitbox.disabled = true
	pass

func is_dead() -> bool:
	return player_state_machine.get_current_state() == "dead"

func _on_player_shot(up: bool):
	# Update crosshair position
	if up:
		gun.position.x = upper_body.position.x
		gun.position.y = upper_body.position.y - 30
	else:
		gun.position.x = upper_body.position.x + (19 * upper_body.scale.x)
		gun.position.y = upper_body.position.y + 4
			
	if gun.shoot(upper_body.scale.x, up):
		gun_shot.emit()
	pass

func _on_player_reloaded():
	if gun.reload():
		player_reloaded.emit()

func _on_player_throw():
	grenade_thrower.throw_grenade(upper_body.scale.x)
	pass

func _on_player_dead():
	player_dead.emit()
	player_state_machine.transition_to_state("dead")

func _on_hit() -> void:
	# Player already dead -- do nothing
	if not is_dead():
		player_state_machine.transition_to_state("dying")
	pass

func _physics_process(delta) -> void:
	player_state_machine.update()
	player_state_machine.physics_update(delta)

	velocity.x = player_state_machine.get_velocity().x
	velocity.y = player_state_machine.get_velocity().y
	
	player_state_machine.set_is_on_floor(is_on_floor())
	update_hitbox(true) if player_state_machine.is_crouched() else update_hitbox(false)
		
	# Flip sprite if moving left
	if velocity.x < 0:
		# Readjust upper body position so that 
		# upper and lower body sprites 'snap'
		# TODO: Is there a better way to do this?
		upper_body.position.x = 7 
		upper_body.scale.x = -1.0
		lower_body.scale.x = -1.0
		full_body.scale.x = -1.0
	elif velocity.x > 0:
		# Readjust upper body position so that 
		# upper and lower body sprites 'snap'
		# TODO: Is there a better way to do this?
		upper_body.position.x = 14 
		upper_body.scale.x = 1.0
		lower_body.scale.x = 1.0
		full_body.scale.x = 1.0
	
	if velocity.x != 0:
		player_moved.emit(self.position.x)

	if player_state_machine.is_full_body():
		switch_full_body(true)
		full_body.play_animation(player_state_machine.get_animation_name())
	else:
		switch_full_body(false)
		upper_body.play_animation(player_state_machine.get_upper_animation_name())
		lower_body.play_animation(player_state_machine.get_lower_animation_name())
	
	# Move based on the velocity and snap to the ground.
	move_and_slide()
