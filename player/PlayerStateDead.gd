class_name PlayerStateDead extends PlayerState

func _ready():
	_id = "dead"
	_animation = "dead"
	full_body_animation = true
	pass

func reset():
	velocity = Vector2.ZERO
	pass

func _process(delta):
	pass
