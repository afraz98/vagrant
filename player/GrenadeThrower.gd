class_name GrenadeThrower extends Marker2D

@onready var throw_timer = get_node("ThrowTimer") as Timer

const grenade = preload("res://player/Grenade.tscn")
const GRENADE_X_VELOCITY = 200.0
const GRENADE_Y_VELOCITY = 400.0

signal spawn_explosion(position: Vector2)

func _on_spawn_explosion(position: Vector2) -> void:
	# Propagate 'spawn_explosion' signal to Level
	spawn_explosion.emit(position)
	pass

func throw_grenade(direction) -> bool:
	if !throw_timer.is_stopped():
		return false

	var grenade_object := grenade.instantiate() as Grenade
	grenade_object.global_position = global_position
	grenade_object.linear_velocity = Vector2(direction * GRENADE_X_VELOCITY, -GRENADE_Y_VELOCITY)
	grenade_object.scale.x = direction
	grenade_object.play_animation("throw")
	grenade_object.set_as_top_level(true)
	grenade_object.spawn_explosion.connect(_on_spawn_explosion)
	add_child(grenade_object)

	throw_timer.start()
	return true
