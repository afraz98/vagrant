class_name PlayerStateFalling extends PlayerState

const FALL_MAX_SPEED_X = 200

func _ready():
	_id = "jump"
	_animation = "fall"
	_upper_animation = "fall"
	_lower_animation = "fall"
	velocity = Vector2.ZERO
	velocity.y = 0
	pass
	
func reset():
	velocity = Vector2.ZERO
	velocity.y = 0
	pass
	
func physics_update(delta):
	# Apply gravity
	velocity.y += gravity * delta

func update():
	if is_on_floor:
		transition_to_state("idle")
	velocity.x = FALL_MAX_SPEED_X * Input.get_axis(&"move_left", &"move_right")	
	pass
