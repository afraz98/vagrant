class_name PlayerStateMachine extends StateMachine

# TODO: This is slowly getting cumbersome to deal with.
# 		probably worth looking into a refactor..
@onready var player_state_idle = get_node("PlayerStateIdle") as PlayerStateIdle
@onready var player_state_crouched = get_node("PlayerStateCrouched") as PlayerStateCrouched
@onready var player_state_crouch_walk = get_node("PlayerStateCrouchWalk") as PlayerStateCrouchWalk

@onready var player_state_walk = get_node("PlayerStateWalk") as PlayerStateWalk
@onready var player_state_jumping = get_node("PlayerStateJumping") as PlayerStateJumping
@onready var player_state_falling = get_node("PlayerStateFalling") as PlayerStateFalling

@onready var player_state_shooting_idle = get_node("PlayerStateShootingIdle") as PlayerStateShootingIdle
@onready var player_state_shooting_crouched = get_node("PlayerStateShootingCrouched") as PlayerStateShootingCrouched
@onready var player_state_shooting_walk = get_node("PlayerStateShootingWalk") as PlayerStateShootingWalk

@onready var player_state_reloading_idle = get_node("PlayerStateReloadingIdle") as PlayerStateReloadingIdle 
@onready var player_state_reloading_walk = get_node("PlayerStateReloadingWalk") as PlayerStateReloadingWalk

@onready var player_state_dying = get_node("PlayerStateDying") as PlayerStateDying
@onready var player_state_dead = get_node("PlayerStateDead") as PlayerStateDead

@onready var player_state_throw = get_node("PlayerStateThrow") as PlayerStateThrow

signal player_shot(bool)
signal player_dead()
signal player_reloaded()
signal player_throw()

func _ready():
	_state = player_state_idle
	_state.transition_state.connect(_on_transition_state)
	pass
	
func is_full_body():
	return _state.full_body_animation

func get_velocity():
	return _state.velocity

func get_is_aiming_up():
	return _state.is_aiming_up

func set_velocity(velocity):
	_state.velocity = velocity
	pass

func set_is_aiming_up(is_aiming_up: bool):
	_state.is_aiming_up = is_aiming_up
	pass

func is_crouched():
	return "crouch" in _state.get_id()

func _on_player_shot(up: bool):
	player_shot.emit(up)

func _on_player_dead():
	if "dead" not in _state._id:
		player_dead.emit()

func _on_player_reloaded():
	_state.player_reloaded.disconnect(_on_player_reloaded)
	player_reloaded.emit()

func _on_player_throw():
	player_throw.emit()

func _on_transition_state(state_id: String):
	# Disconnect current signals
	_state.transition_state.disconnect(_on_transition_state)
	
	if _state.has_signal("player_shot"):
		_state.player_shot.disconnect(_on_player_shot)
	
	if _state.has_signal("player_throw"):
		_state.player_throw.disconnect(_on_player_throw)
	
	# Retrieve previous velocity from current state
	var previous_velocity = get_velocity()
	var aiming_up = get_is_aiming_up()
	
	# TODO: This is getting bloated again. 
	#		Might make more sense to set the _state variable to get_node()...
	if state_id == "idle":
		_state = player_state_idle
	elif state_id == "crouch_idle":
		_state = player_state_crouched
	elif state_id == "walk":
		_state = player_state_walk
	elif state_id == "crouch_walk":
		_state = player_state_crouch_walk
	elif state_id == "jump":
		_state = player_state_jumping
	elif state_id == "fall":
		_state = player_state_falling
	elif state_id == "shooting_idle":
		_state = player_state_shooting_idle
		_state.player_shot.connect(_on_player_shot)
	elif state_id == "shooting_walk":
		_state = player_state_shooting_walk
		_state.player_shot.connect(_on_player_shot)
	elif state_id == "reload_idle":
		_state = player_state_reloading_idle
		_state.player_reloaded.connect(_on_player_reloaded)
	elif state_id == "reload_walk":
		_state = player_state_reloading_walk
		_state.player_reloaded.connect(_on_player_reloaded)
	elif state_id == "shooting_crouched":
		_state = player_state_shooting_crouched
		_state.player_shot.connect(_on_player_shot)
	elif state_id == "dying":
		_state = player_state_dying
		_state.player_dead.connect(_on_player_dead)
	elif state_id == "dead":
		_state = player_state_dead
	elif state_id == "throw":
		_state = player_state_throw
		_state.player_throw.connect(_on_player_throw)

	# Provide new state with previous attributes
	set_is_aiming_up(aiming_up)
	set_velocity(previous_velocity)
	
	# Connect new transition signal
	_state.transition_state.connect(_on_transition_state)
			
	# Call state's reset function before processing begins
	_state.reset()
