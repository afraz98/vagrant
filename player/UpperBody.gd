class_name UpperBody extends Sprite2D

@onready var timer = get_node("Timer") as Timer
@onready var animation_player = get_node("AnimationPlayer") as AnimationPlayer

var animation_timeouts = {
	'up_shoot': 0.1,
	'up_idle': 0.1,
	'idle': 0.1,
	'shoot': 0.1,
	'reload': 1.9,
	'throw': 0.6,
	'stab': 1.0,
	'walk': 0.1,
	'fall': 0.1,
	'jump': 0.1,
}

func play_animation(animation: String) -> void:
	if timer.is_stopped():
		timer.wait_time = animation_timeouts[animation]
		timer.start()
		animation_player.play(animation)
