class_name HidingEnemy extends Enemy

@onready var hiding_enemy_sprite = get_node("HidingEnemySprite") as HidingEnemySprite

func get_player_proximity() -> float:
	return self.position.x - player_position

func _on_hit():
	_state_machine.transition_to_state("death")

func _on_player_death():
	pass

func _ready():
	_state_machine = get_node("HidingEnemyStateMachine") as HidingEnemyStateMachine
	walking_direction = -1.0
	pass

func _physics_process(delta):
	# Update player proximity
	_state_machine.update_player_proximity(get_player_proximity())
	
	# Process current state
	_state_machine.update()
	_state_machine.physics_update(delta)

	velocity.x = _state_machine.get_velocity().x
	velocity.y = _state_machine.get_velocity().y
	
	if is_on_wall():
		velocity.x = -velocity.x
	
	# Flip sprite if moving left		
	if velocity.x != 0:
		$HidingEnemySprite.flip_h = velocity.x < 0
	
	# Clamp to the maximum horizontal movement speed.
	velocity.x = clamp(velocity.x, -WALK_MAX_SPEED, WALK_MAX_SPEED)

	# Move based on the velocity and snap to the ground.
	move_and_slide()
	
	hiding_enemy_sprite.play_animation(_state_machine.get_animation_name())
	pass
