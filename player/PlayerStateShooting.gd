class_name PlayerStateShootingIdle extends PlayerState

signal player_shot(bool)

func _ready():
	_id = "shooting"
	_upper_animation = "shoot"
	_lower_animation = "idle"
	pass

func reset():
	velocity = Vector2.ZERO
	_upper_animation = "shoot"
	_lower_animation = "idle"
	pass

func update():
	if Input.is_action_pressed("up"):
		is_aiming_up = true		
	elif Input.is_action_just_released("up"):
		is_aiming_up = false
	
	if Input.is_action_pressed("crouch"):
		transition_to_state("shooting_crouched")
	
	_upper_animation = "up_shoot" if is_aiming_up else "shoot"	
	player_shot.emit(is_aiming_up)
	
	if Input.is_action_pressed("move_left") or Input.is_action_pressed("move_right"):
		transition_to_state("shooting_walk")
	
	if Input.is_action_just_released("shoot"):
		transition_to_state("idle")
	pass
