class_name RiflemanStateShoot extends EnemyState

signal enemy_shot()

func _ready():
	_id = "shoot"
	_animation = "shoot"

func update():
	enemy_shot.emit()
	transition_to_state("alert")
	
func physics_update(delta):
	pass

