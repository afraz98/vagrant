class_name PlayerStateJumping extends PlayerState

const JUMP_MAX_SPEED_Y = 250
const JUMP_MAX_SPEED_X = 200

func _ready():
	_id = "jump"
	_animation = "fall"
	_upper_animation = "fall"
	_lower_animation = "fall"
	velocity = Vector2.ZERO
	velocity.y = -JUMP_MAX_SPEED_Y 
	pass
	
func reset():
	velocity = Vector2.ZERO
	velocity.y = -JUMP_MAX_SPEED_Y
	pass
	
func physics_update(delta):
	# Vertical movement code. Apply gravity.
	velocity.y += gravity * delta

func update():
	if velocity.y > 0:
		transition_to_state("fall")
	
	velocity.x = JUMP_MAX_SPEED_X * Input.get_axis(&"move_left", &"move_right")	
	pass
