class_name Explosion extends CharacterBody2D

var collision_count: int = 0
var max_collisions: int = 4

func play_animation(animation):
	$AnimationPlayer.play(animation)

func explode():
	play_animation("explode")

func _on_body_entered(body: Node) -> void:
	print("collided")
	if body is CharacterBody2D:
		if not (body as CharacterBody2D).is_dead():
			(body as CharacterBody2D).destroy()
	explode()

func _ready():
	velocity.x = 0
	velocity.y = 0

func _physics_process(delta):
	collision_count = 0
	var collision = move_and_collide(velocity)

	while(collision and collision_count < max_collisions):
		var collider = collision.get_collider()
		if collider is Enemy:
			collider._on_hit()
			break
		else:
			var normal = collision.get_normal()
			var remainder = collision.get_remainder()
			velocity = velocity.bounce(normal)
			remainder = remainder.bounce(normal)
			
			collision_count += 1
			collision = move_and_collide(remainder)

