class_name DoorStateOpen extends State

var player_proximity: float = 0.0


func _ready():
	_id = "open"
	_animation = "open"
	pass

func _process(delta):
	pass

func update_player_proximity(player_proximity):
	self.player_proximity = player_proximity
	pass
