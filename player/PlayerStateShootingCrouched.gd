class_name PlayerStateShootingCrouched extends PlayerState

signal player_shot(bool)

func _ready():
	_id = "shooting_crouched"
	_animation = "crouch_shoot"
	full_body_animation = true
	pass

func reset():
	velocity = Vector2.ZERO
	pass

func update():
	player_shot.emit(false)
	
	if Input.is_action_just_released("crouch"):
		transition_to_state("shooting_idle")
	
	if Input.is_action_just_released("shoot"):
		transition_to_state("crouch_idle")
	pass
