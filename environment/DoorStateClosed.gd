class_name DoorStateClosed extends State

var player_proximity: float = 0.0

func _ready():
	_id = "closed"
	_animation = "closed"
	pass

func _process(delta):
	if abs(self.player_proximity) <= 100:
		transition_to_state("opening")
	pass

func update_player_proximity(player_proximity):
	self.player_proximity = player_proximity
	pass
