class_name PlayerStateThrow extends PlayerState

signal player_throw()

func _ready():
	_id = "throw"
	_upper_animation = "throw"
	_lower_animation = "idle"
	pass

func reset():
	velocity = Vector2.ZERO
	pass

func _process(delta):
	player_throw.emit()
	transition_to_state("idle")
	pass
