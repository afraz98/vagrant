class_name Door extends CharacterBody2D

@onready var door_sprite = get_node("DoorSprite") as DoorSprite

var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")

var _state_machine: DoorStateMachine
var player_position: int = 0
var player_proximity: float

func _ready():
	_state_machine = get_node("DoorStateMachine") as DoorStateMachine

func open_door():
	_state_machine.transition_to_state("open")

func get_player_proximity() -> float:
	return self.position.x - player_position

func update_player_position(pos_x):
	player_position = pos_x

func _physics_process(delta):
	# Update player proximity
	_state_machine.update_player_proximity(get_player_proximity())
	
	# Process current state
	_state_machine.update()
	_state_machine.physics_update(delta)

#	velocity.x = _state_machine.get_velocity().x
#	velocity.y = _state_machine.get_velocity().y
	
	# Move based on the velocity and snap to the ground.
	move_and_slide()	
	door_sprite.play_animation(_state_machine.get_animation_name())

func _on_hit():
	pass
