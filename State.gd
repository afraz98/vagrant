class_name State extends Node

signal transition_state(next_state: String)

var _parent = get_parent()
var _id: String = ""
var _animation: String = ""
var _wait_time: float = -1
var cycles: int = 0

func get_id():
	return _id

func get_animation_name():
	return _animation

func get_wait_time():
	return _wait_time

func set_parent(parent):
	_parent = parent

func reset():
	cycles = 0
	pass

func _ready():
	pass

func _on_timer_timeout():
	pass

func update():
	pass

func physics_update(delta):
	pass

func transition_to_state(next_state_id: String):
	transition_state.emit(next_state_id)
