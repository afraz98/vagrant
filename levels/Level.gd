class_name Level extends Node2D

const Explosion = preload("res://environment/Explosion.tscn")

@onready var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")

@onready var player = get_node("Player") as Player
@onready var hiding_enemy = get_node("HidingEnemy") as HidingEnemy
@onready var stationary_enemy = get_node("StationaryEnemy") as StationaryEnemy
@onready var stationary_enemy_2 = get_node("StationaryEnemy2") as StationaryEnemy
@onready var rifleman = get_node("Rifleman") as Rifleman
@onready var exit_door = get_node("Door") as Door

signal game_over()

var enemies := []

func _ready():
	enemies.append(hiding_enemy)
	enemies.append(stationary_enemy)
	enemies.append(stationary_enemy_2)
	enemies.append(rifleman)

	player.player_dead.connect(_on_player_death)
	player.player_moved.connect(_on_player_moved)
	$Player/GrenadeThrower.spawn_explosion.connect(_on_spawn_explosion)
		
	for enemy in enemies:
		enemy.enemy_killed.connect(_on_enemy_killed)

func _on_player_death():
	for enemy in enemies:
		enemy._on_player_death()
	game_over.emit()
	pass

func _on_player_moved(pos_x: int):
	for enemy in enemies:
		enemy.update_player_position(pos_x)
	exit_door.update_player_position(pos_x)

func broadcast_death(dead_enemy: Enemy):
	# Broadcast a death to all actors on the level
	for enemy in enemies:
		enemy._enemy_died_nearby(dead_enemy)
	pass

func _on_enemy_killed(enemy: Enemy):
	broadcast_death(enemy)
	enemies.erase(enemy)
	pass

func _on_spawn_explosion(position: Vector2):
	var explosion := Explosion.instantiate() as Explosion
	explosion.global_position = position
	explosion.set_as_top_level(true)
	add_child(explosion)
	explosion.explode()
	pass
