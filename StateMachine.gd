class_name StateMachine extends Node

var _parent: Node = null
var _state: State = null

# Signal indicating that the current state has changed. 
signal state_changed(state: State)

func _ready():
	_parent = get_parent()
	pass

func _on_transition_state(state_id: String):
	pass

func transition_to_state(state_id: String):
	_on_transition_state(state_id)

func update():
	if _state != null:
		_state.update()
	pass

func set_state(state: State):
	_state = state

func set_is_on_floor(is_on_floor: bool) -> void:
	_state.set_is_on_floor(is_on_floor)
	pass

func get_animation_name():
	if _state != null:
		return _state.get_animation_name()

func get_upper_animation_name():
	if _state != null:
		return _state.get_upper_animation_name()

func get_lower_animation_name():
	if _state != null:
		return _state.get_lower_animation_name()

func get_current_state():
	if _state != null:
		return _state._id

func physics_update(delta):
	if _state != null:
		_state.physics_update(delta)
	pass
