class_name DroneStateStrafe extends EnemyState

# Strafe-bombing state of the Drone enemy. Should travel left and right 
# from initial position several cycles before entering another attack state. 

func _ready():
	# Number of cycles the Drone has remained in this state
	cycles = 0
	_animation = "walk"
	pass

func reset():
	pass

func _process(delta):
	# Increment cycles in state
	cycles += 1
	pass
