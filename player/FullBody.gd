class_name FullBody extends Sprite2D

@onready var timer = get_node("Timer") as Timer
@onready var animation_player = get_node("AnimationPlayer") as AnimationPlayer

var animation_timeouts = {
	'crouch_idle': 0.1,
	'crouch_shoot': 0.1,
	'crouch_throw': 0.5,
	'crouch_walk': 0.1,
	'death': 1.9,
	'dead': 0.1,
}

func play_animation(animation: String) -> void:
	if timer.is_stopped() and animation != animation_player.current_animation:		
		timer.wait_time = animation_timeouts[animation]
		timer.start()
		animation_player.play(animation)
