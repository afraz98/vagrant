class_name DroneStateAttacking extends EnemyState

@onready var lift: float = 0.0

func _ready():
	_id = "attack"
	_animation = "attack"
	pass

func reset():
	velocity = Vector2.ZERO

func update():
	pass

func physics_update(delta):
	lift = delta * -gravity
	velocity.y += (delta * (gravity)) + lift 
	pass
