class_name Rifle extends Marker2D

@onready var reload_timer = get_node("ReloadTimer") as Timer
@onready var cooldown_timer = get_node("CooldownTimer") as Timer

const bullet = preload("res://environment/Bullet.tscn")
const BULLET_VELOCITY = 1000.0

# Maximum ammo capacity for the pistol
const RIFLE_MAGAZINE_CAPACITY = 1
var ammo: int = 0

func _ready():
	ammo = RIFLE_MAGAZINE_CAPACITY
	pass

func reload() -> bool:
	if not reload_timer.is_stopped():
		# In the middle of a reload already -- cannot reload.
		return false
	reload_timer.start()
	return true

func _reload_finished():
	reload_timer.stop()
	ammo = RIFLE_MAGAZINE_CAPACITY

func is_empty() -> bool:
	return ammo == 0

func spawn_projectile():
	pass

func shoot(direction: float) -> bool:
	if not cooldown_timer.is_stopped():
		# Already shooting -- wait for cooldown to elapse.
		return false
	
	if ammo == 0:
		# TODO: Probably best to play some form of sound effect
		# indicating the enemy is out of ammo.
		return false	

	elif not reload_timer.is_stopped():
		# In the middle of reloading -- should not be able to shoot.
		return false

	var bullet_projectile := bullet.instantiate() as Bullet
		
	bullet_projectile.global_position = global_position
	bullet_projectile.linear_velocity = Vector2(direction * BULLET_VELOCITY, 0.0)
	bullet_projectile.scale.x = direction
	
	bullet_projectile.set_as_top_level(true)
	add_child(bullet_projectile)
	
	# Decrease current ammo
	ammo -= 1
	
	cooldown_timer.start()
	return true
