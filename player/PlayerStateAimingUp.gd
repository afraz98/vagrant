class_name PlayerStateAimingUp extends PlayerState

func _ready():
	_id = "reload_idle"
	_upper_animation = "up_idle"
	_lower_animation = "idle"

func reset():
	pass

func update():
	if Input.is_action_pressed("shoot"):
		transition_to_state("shoot_up")

	if Input.is_action_just_released("up"):
		transition_to_state("idle")
	pass
