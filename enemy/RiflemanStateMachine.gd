class_name RiflemanStateMachine extends StateMachine

@onready var rifleman_state_idle = get_node("RiflemanStateIdle") as RiflemanStateIdle
@onready var rifleman_state_alert = get_node("RiflemanStateAlert") as RiflemanStateAlert
@onready var rifleman_state_aim = get_node("RiflemanStateAim") as RiflemanStateAim
@onready var rifleman_state_shoot = get_node("RiflemanStateShoot") as RiflemanStateShoot
@onready var rifleman_state_reload = get_node("RiflemanStateReload") as RiflemanStateReload
@onready var rifleman_death_state = get_node("RiflemanDeathState") as RiflemanDeathState
@onready var state_timer = get_node("StateTimer") as Timer

signal enemy_shot()
signal enemy_reloaded()

func _ready():
	_state = rifleman_state_idle
	_state.transition_state.connect(_on_transition_state)
	pass

func set_parent(parent):
	_parent = parent

func shoot():
	return _parent.shoot()
	pass

func out_of_ammo() -> bool:
	return _parent.out_of_ammo()

func start_state_timer(wait_time):
	if wait_time > 0:
		state_timer.wait_time = wait_time
		state_timer.timeout.connect(_state._on_timer_timeout)
		state_timer.start()
	pass

func update_player_proximity(player_proximity):
	_state.update_player_proximity(player_proximity)

func get_velocity():
	return _state.velocity

func _on_enemy_shot():
	_state.enemy_shot.disconnect(_on_enemy_shot)
	enemy_shot.emit()

func _on_enemy_reloaded():
	_state.enemy_reloaded.disconnect(_on_enemy_reloaded)
	enemy_reloaded.emit()
	pass

func _on_transition_state(state_id: String):
	# Disconnect current transition signal
	_state.transition_state.disconnect(_on_transition_state)

	if state_id == "idle":
		_state = rifleman_state_idle
	elif state_id == "alert":
		_state = rifleman_state_alert
	elif state_id == "aim":
		_state = rifleman_state_aim
	elif state_id == "shoot":
		_state = rifleman_state_shoot
		_state.enemy_shot.connect(_on_enemy_shot)
	elif state_id == "death":
		_state = rifleman_death_state
	elif state_id == "reload":
		_state = rifleman_state_reload
		_state.enemy_reloaded.connect(_on_enemy_reloaded)
	
	_state.transition_state.connect(_on_transition_state)	# Connect new transition signal
	start_state_timer(_state.get_wait_time()) 				# Start state timer
	_state.set_parent(self)
	
	# Call state's reset function before processing begins
	_state.reset()	
	pass
