class_name StationaryEnemyStateFear extends EnemyState

var player_position = 0

func _ready():
	_id = "fear"
	_animation = "fear"
	_wait_time = 1.2

func update_player_position(player_position):
	self.player_position = player_position
	pass

func update():
	pass
	
func _on_timer_timeout():
	transition_to_state("flee")
	
