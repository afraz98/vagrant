class_name HidingEnemyFearState extends EnemyState

func _ready():
	_id = "fear"
	_animation = "fear"
	_wait_time = 1.2

func update():
	pass
	
func _on_timer_timeout():
	transition_to_state("flee")
	
