class_name PlayerStateDying extends PlayerState

signal player_dead()

func _ready():
	_id = "dying"
	_animation = "death"
	full_body_animation = true
	pass

func reset():
	velocity = Vector2.ZERO
	pass

func _process(delta):
	player_dead.emit()
	pass
