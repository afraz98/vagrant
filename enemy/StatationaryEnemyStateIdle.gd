class_name StationaryEnemyStateIdle extends EnemyState

var player_position

func _ready():
	_id = "idle"
	_animation = "idle"
	velocity = Vector2.ZERO
	player_position = INF
	pass

func reset():
	velocity = Vector2.ZERO
	player_position = INF
	pass
	
func update_player_position(player_position):
	self.player_position = player_position

func update():
	pass
	
