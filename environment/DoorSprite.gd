class_name DoorSprite extends Sprite2D

@onready var animation_player = get_node("AnimationPlayer") as AnimationPlayer

func play_animation(animation: String):
	animation_player.play(animation)
	pass
