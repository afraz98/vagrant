class_name PlayerStateIdle extends PlayerState

func _ready():
	_id = "idle"
	_animation = "idle"
	_upper_animation = "idle"
	_lower_animation = "idle"
	velocity = Vector2.ZERO
	pass

func reset():
	velocity = Vector2.ZERO
	_upper_animation = "idle"
	_lower_animation = "idle"
	is_aiming_up = false
	pass
	
func update():
	if Input.is_action_pressed("crouch"):
		transition_to_state("crouch_idle")
	
	if Input.is_action_pressed("up"):
		is_aiming_up = true
		_upper_animation = "up_idle"
	
	if Input.is_action_pressed("move_left") or Input.is_action_pressed("move_right"):
		transition_to_state("walk")
		
	if Input.is_action_pressed("jump"):
		transition_to_state("jump")

	if Input.is_action_pressed("shoot"):
		transition_to_state("shooting_idle")

	if Input.is_action_pressed("reload"):
		transition_to_state("reload_idle")

	if Input.is_action_pressed("throw"):
		transition_to_state("throw")

	if Input.is_action_just_released("up"):
		is_aiming_up = false
		_upper_animation = "idle"
	pass


