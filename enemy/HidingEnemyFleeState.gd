class_name HidingEnemyFleeState extends EnemyState

# Maximum x velocity while walking
const WALK_MAX_SPEED = 150

func _ready():
	_id = "flee"
	_animation = "flee"
	velocity = Vector2.ZERO
	pass

func update():
	velocity.x = WALK_MAX_SPEED
	pass
	
func reset():
	velocity = Vector2.ZERO
	
func physics_update(delta):
	pass
