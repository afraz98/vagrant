class_name PlayerStateShootingWalk extends PlayerState

const WALK_MAX_SPEED = 200

signal player_shot(bool)

func _ready():
	_id = "shooting_walk"
	_upper_animation = "shoot"
	_lower_animation = "walk"
	pass

func reset():
	pass

func update():
	if Input.is_action_pressed("up"):
		is_aiming_up = true		
	elif Input.is_action_just_released("up"):
		is_aiming_up = false
		
	if Input.is_action_pressed("crouch"):
		transition_to_state("shooting_crouched")
		
	_upper_animation = "up_shoot" if is_aiming_up else "shoot"	
	player_shot.emit(is_aiming_up)
	
	if Input.is_action_just_released("shoot"):
		transition_to_state("walk")
	elif Input.is_action_just_released("move_left") or Input.is_action_just_released("move_right"):
		transition_to_state("shooting_idle")

	velocity.x = WALK_MAX_SPEED * Input.get_axis(&"move_left", &"move_right")
	pass
