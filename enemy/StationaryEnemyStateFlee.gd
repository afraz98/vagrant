class_name StationaryEnemyStateFlee extends EnemyState

# Maximum x velocity while walking
const WALK_MAX_SPEED = 150

var _player_position: int = 0

func _ready():
	_id = "flee"
	_animation = "flee"
	velocity = Vector2.ZERO
	pass

func update_player_position(player_position):
	self._player_position = player_position
	pass

func update():
	velocity.x = WALK_MAX_SPEED
	pass
	
func reset():
	velocity = Vector2.ZERO
	
